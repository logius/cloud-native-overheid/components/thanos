Thanos
=========

This repository contains an Ansible role for installing the Thanos monitoring stack and additonal componennts.

References:
* Chart: https://github.com/bitnami/charts/tree/master/bitnami/thanos
* Thanos: https://thanos.io/
* Overview with a clear picture of the components and their relations: https://thanos.io/tip/thanos/quick-tutorial.md/
* Thanos sidecar in Prometheus: https://prometheus-operator.dev/docs/operator/thanos/
* Release notes: https://thanos.io/tip/thanos/changelog.md/

Summary
=========

Thanos Query implements the Prometheus HTTP v1 API to query data in a Thanos cluster via PromQL.
It gathers data from underlying StoreAPIs. StoreAPIs are endpoints containing Prometheus data.

Sidecar
=========

In order to make use of Thanos to query your Prometheus instances, make sure to deploy the Thanos Sidecar with each Prometheus. Add the following to the `prometheus` object, under `.spec`:

```
  thanos:
    baseImage: bitnami/thanos
    version: {{ THANOS_VERSION }}-scratch-r0
```

Documentation: https://prometheus-operator.dev/docs/operator/thanos/

When the Compactor and S3 storage is enabled, also add the following to `.spec.thanos`:

```
    objectStorageConfig:
      # Secret to be copied from the cluster where Thanos is deployed
      key: objstore.yml
      name: {{ THANOS_INSTANCE_NAME }}-thanos-objstore-secret
```

Optional, but advised when offloading the metrics to central storage is this:

```
    minTime: -24h
```

That makes sure that only queries relating to data _less than_ 24 hours in the past will be queried in the local Prometheus data. Since all metrics older than two hours is moved to the central storage, it makes no sense to bother the Prometheus instances with queries for older data. Also, the `.spec.retention` in Prometheus can be restricted to e.g. 24 hours.

On the other hand it is wise to set `.spec.thanos.minTime` and `.spec.retention` not _too_ strict: otherwise, in case of a downtime of S3 for a couple of hours, you might lose data.

T.B.D
=========
* Option to generate certificates for encrypted connection betweend query and query-hub

