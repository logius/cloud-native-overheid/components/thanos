global:
  imageRegistry: "{{ image_registry | default() }}"

{% if config.thanos.basic_auth_enabled %}
auth:
  basicAuthUsers:
    admin: "{{ admin_basic_auth_pass }}"
{% endif %}
query:
{% if config.thanos.query.ingress_enabled %}
  ingress:
    enabled: true
    tls: true
    certManager: "{{ config.system.use_certmanager }}"
    hostname: "{{ config.thanos.query.fqdn }}"
    annotations: 
      cert-manager.io/cluster-issuer: {{ config.certmanager.issuer | default('none') }}
{% endif %}
{% if config.thanos.query.replicalabel is defined %}
  replicaLabel: "{{ config.thanos.query.replicalabel }}"
{% endif %}

  # https://thanos.io/tip/thanos/changelog.md/#v0240httpsgithubcomthanos-iothanostreerelease-024---20211222
  # https://github.com/bitnami/charts/issues/11127
  # In Thanos, --store is deprecated and --endpoint replaces the option; in CNO endpoints is also changed to stores; in Bitnami it is not yet changed
  stores: [
  {%- for endpoint in item.endpoints %}
    "{{ endpoint }}"{% if not loop.last %},{% endif %}
  {% endfor %}
  ]

  grpc:
    client:
      serverName: "{{ item.grpc_client_remote_hostname | default() }}"
      tls:
{% if item.grpc_client_secure_endpoint | default(false) %}
        enabled: true
        existingSecret:
          name: "{{ item.name }}-grpc-client-secret"
{% else %}
        enabled: false
{% endif %}
    server:
      tls:
        enabled: false
  
  service:
    type: "{{ item.service_type }}"
{% if item.service_annotations is defined %}
    annotations: {{ item.service_annotations }}
{% endif %}

  serviceGrpc:
    type: "{{ item.service_grpc_type | default('ClusterIP') }}"
{% if item.service_grpc_loadBalancerIP is defined %}
    loadBalancerIP: {{ item.service_grpc_loadBalancerIP }}
{% endif %}
{% if item.service_grpc_lb_source_ranges is defined %}
    loadBalancerSourceRanges: {{ item.service_grpc_lb_source_ranges }}
{% endif %}

{% if item.service_grpc_annotations is defined %}
    annotations: {{ item.service_grpc_annotations }}
{% endif %}

queryFrontend:
  enabled: {{ item.query_frontend_enabled | default(true) }}

compactor:
  enabled: {{ item.compactor_enabled | default(false) }}
{% if item.compactor_enabled | default(false) %} 
  podAnnotations:
    backup.velero.io/backup-volumes: "data"
  strategyType: Recreate
  persistence:
    size: {{ item.compactor.pvc_size | default("8Gi") }}
  retentionResolutionRaw: {{ item.compactor.retentionResolutionRaw | default("0s") }}
  retentionResolution5m: {{ item.compactor.retentionResolution5m | default("0s") }}
  retentionResolution1h: {{ item.compactor.retentionResolution1h | default("0s") }}
  extraFlags: {{ item.compactor_extra_flags | default([]) }}
{% endif %}

storegateway:
  podAnnotations:
    backup.velero.io/backup-volumes: "data"
  enabled: {{ item.storegateway_enabled | default(false) }}
{% if item.storegateway_enabled | default(false) %} 
  extraFlags: {{ item.storegateway_extra_flags | default(["--max-time=-4h"]) }}
{% endif %}

thanos:
  createObjstoreSecret: {{ item.s3_storage_enabled | default(false) }}

metrics:
  enabled: {{ item.compactor_enabled | default(false) }}
  serviceMonitor:
    enabled: {{ item.compactor_enabled | default(false) }}

{% if item.s3_storage_enabled | default(false) %}
objstoreConfig: |
  type: S3
  config:
    bucket: "{{ config.thanos.objectstore.bucket_name | default(platform.prefix + '-metrics') }}"
    endpoint: "{{ config.thanos.objectstore.fqdn | default(config.backup_vault.fqdn) }}"
    region: ""
    access_key: "{{ minio_access_key }}"
    insecure: false
    signature_version2: false
    secret_key: "{{ minio_secret_key }}"
    put_user_metadata: {}
    http_config:
      idle_conn_timeout: 1m30s
      response_header_timeout: 2m
      insecure_skip_verify: false
      tls_handshake_timeout: 10s
      expect_continue_timeout: 1s
      max_idle_conns: 100
      max_idle_conns_per_host: 100
      max_conns_per_host: 0
    trace:
      enable: false
    list_objects_version: ""
    part_size: 67108864
    sse_config:
      type: ""
      kms_key_id: ""
      kms_encryption_context: {}
      encryption_key: ""
{% endif %}
